package pl.sda.crmspring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.crmspring.model.entity.AppUser;
import pl.sda.crmspring.service.AppUserAuthenticationService;
import pl.sda.crmspring.service.AppUserService;

import java.util.Optional;

@Controller
@RequestMapping("/user")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    public AppUserAuthenticationService appUserAuthenticationService;

    public AppUserController(AppUserService userService) {
        this.appUserService = appUserService;
    }

    @GetMapping(path = "/profile")
    public String getProfile() {
        return "authorization/profile";
    }

    @GetMapping(path = "/tasks")
    public String getTasksView(Model model) {
        Optional<AppUser> appUserOptional = appUserAuthenticationService.getLoggedInUser();
        if (appUserOptional.isPresent()) {
            model.addAttribute("tasks", appUserOptional.get().getTasks());
            return "tasks";
        }
        return "redirect:/login";

    }

    @GetMapping(path = "/accounts")
    public String getAccountsView(Model model) {
        Optional<AppUser> appUserOptional = appUserAuthenticationService.getLoggedInUser();
        if (appUserOptional.isPresent()) {
            model.addAttribute("accounts", appUserOptional.get().getAccounts());
            return "accounts";
        }
        return "redirect:/login";
    }

}
