package pl.sda.crmspring.controller.rest;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.crmspring.model.dto.AccountDto;
import pl.sda.crmspring.model.dto.TaskDto;
import pl.sda.crmspring.model.entity.Account;
import pl.sda.crmspring.service.AccountService;
import pl.sda.crmspring.service.TaskService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/accounts")
public class AccountRestController {

    private AccountService accountService;

    public AccountRestController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public List<AccountDto> getAll() {
        return accountService.getAllAccounts();
    }

    @PostMapping
    public void create(@RequestBody AccountDto accountDto) {
        accountService.addAccount(accountDto);
    }


    @GetMapping("/{id}")
    public AccountDto getOne(@PathVariable Long id) {
        return accountService.getAccountById(id).get();
    }

//    @PutMapping("/{id}")
//    public void update(@PathVariable Long id, @RequestBody TaskDto task) {
//        taskService.update(id,task);
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@PathVariable Long id) {
//        taskService.delete(id);
//    }

}
