package pl.sda.crmspring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.crmspring.model.dto.AccountDto;
import pl.sda.crmspring.model.dto.RequestAccountAndAddress;
import pl.sda.crmspring.model.entity.Account;
import pl.sda.crmspring.service.AccountService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/accounts")
public class AccountController {

    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/addaccount")
    public String addAccount(Model model) {
        return "addaccount";
    }

    @GetMapping
    public String listAllAccounts (Model model) {
        List<AccountDto> accountDtos = accountService.getAllAccounts();
        model.addAttribute("accounts",accountDtos);
        return "accounts";
    }

    @PostMapping
    public String sendNewAccount (Model model, RequestAccountAndAddress request) {
        Optional<Account> accountOptional = accountService.addAccountWithAddress(request);
        if (accountOptional.isPresent()) {
            Account createdAccount = accountOptional.get();
            return "redirect:/accounts/";
        }
        model.addAttribute("message", "Cannot add this account!");
        model.addAttribute("formObject", request);

        return "accounts";
    }


}
