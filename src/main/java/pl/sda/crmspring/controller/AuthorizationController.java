package pl.sda.crmspring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.crmspring.model.dto.AppUserDto;
import pl.sda.crmspring.model.entity.AppUser;
import pl.sda.crmspring.service.AppUserService;

import java.util.Optional;

@Controller
public class AuthorizationController {

    @Autowired
    private AppUserService appUserService;

    @GetMapping("/")
    public String getWelcomePage() { return "welcome"; }

    @GetMapping("/login")
    public String getLoginPage() {
        return "authorization/login";
    }


    @GetMapping("/register")
    public String getRegisterPage(Model model) {
        model.addAttribute("formObject", new AppUserDto());

        return "authorization/register";
    }

    @PostMapping("/register")
    public String sendRegister(Model model, AppUserDto request) {

        Optional<AppUser> appUserOptional = appUserService.register(request);
        if (appUserOptional.isPresent()) {
            return "redirect:/login";
        }

        model.addAttribute("formObject", request);
        model.addAttribute("message", "Unable to register!");

        return "authorization/register";
    }
}
