package pl.sda.crmspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.crmspring.model.dto.AppUserDto;
import pl.sda.crmspring.model.entity.AppUser;
import pl.sda.crmspring.repository.AccountRepository;
import pl.sda.crmspring.repository.ProjectRepository;
import pl.sda.crmspring.repository.TaskRepository;
import pl.sda.crmspring.repository.AppUserRepository;

import java.util.Optional;

@Service
public class AppUserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private UserRoleService userRoleService;

    public Optional<AppUser> register(AppUserDto appUserDto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findByUsername(appUserDto.getUsername());
        if (optionalAppUser.isPresent()) {
            return Optional.empty();
        }
        AppUser appUser = new AppUser();
        appUser.setUsername(appUserDto.getUsername());
        appUser.setPassword(passwordEncoder.encode(appUserDto.getPassword()));
        appUser.setFirstName(appUserDto.getFirstName());
        appUser.setLastName(appUserDto.getLastName());
        appUser.setRoles(userRoleService.getDefaultUserRoles());

        return Optional.of(appUserRepository.save(appUser));
    }

    public Optional<AppUser> findById(Long id) {
        return appUserRepository.findById(id);
    }




}
