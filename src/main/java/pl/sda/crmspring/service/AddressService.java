package pl.sda.crmspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.crmspring.model.dto.AccountDto;
import pl.sda.crmspring.model.dto.AddressDto;
import pl.sda.crmspring.model.entity.Account;
import pl.sda.crmspring.model.entity.Address;
import pl.sda.crmspring.model.entity.AppUser;
import pl.sda.crmspring.repository.AccountRepository;
import pl.sda.crmspring.repository.AddressRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    AccountRepository accountRepository;

    public List<Address> getAllAddresses() {
        return addressRepository.findAll();
    }

    public Optional<Address> addAddress(AddressDto addressDto) {
        Optional<Address> addressOptional = addressRepository.findByCityAndAndStreetAndHouseNumber(
                addressDto.getCity(),addressDto.getStreet(),addressDto.getHouseNumber());
        if (!addressOptional.isPresent()) {
            Address address = new Address();
            address.setCity(addressDto.getCity());
            address.setStreet(addressDto.getStreet());
            address.setHouseNumber(addressDto.getHouseNumber());
            address.setPostCode(addressDto.getPostCode());

            return Optional.of(addressRepository.save(address));
        }
        return Optional.empty();
    }

    public Optional<Account> addAddressToAccount(Long accountId, Long addressId) {
        Optional<Account> optionalAccount = accountRepository.findById(accountId);
        if(optionalAccount.isPresent()) {
            Optional<Address> optionalAddress = addressRepository.findById(addressId);
            if(optionalAddress.isPresent()) {
                Account account = optionalAccount.get();
                Address address = optionalAddress.get();
                account.setAddress(address);

                return Optional.of(accountRepository.save(account));
            }
            return Optional.empty();
        }
        return Optional.empty();
    }
}
