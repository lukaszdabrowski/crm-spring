package pl.sda.crmspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.sda.crmspring.model.entity.UserRole;
import pl.sda.crmspring.repository.UserRoleRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserRoleService {

    @Value("${crm.user.defaultRoles}")
    private String[] defaultRoles;

    @Autowired
    UserRoleRepository userRoleRepository;

    public Set<UserRole> getDefaultUserRoles(){
        Set<UserRole> userRoles = new HashSet<>();
        for (String role : defaultRoles) {
            Optional<UserRole> singleRole = userRoleRepository.findByName(role);
            if (singleRole.isPresent()) {
                userRoles.add(singleRole.get());
            }
        }
        return userRoles;
    }
}
