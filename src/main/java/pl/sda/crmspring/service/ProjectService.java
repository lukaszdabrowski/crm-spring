package pl.sda.crmspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.crmspring.model.dto.AccountDto;
import pl.sda.crmspring.model.dto.ProjectDto;
import pl.sda.crmspring.model.entity.Account;
import pl.sda.crmspring.model.entity.Project;
import pl.sda.crmspring.repository.ProjectRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    AppUserService appUserService;

    @Autowired
    AppUserAuthenticationService appUserAuthenticationService;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<ProjectDto> getAllProjects() {
        List<Project> projectOptionals = projectRepository.findAll();
        List<ProjectDto> projectDtos = projectOptionals
                .stream()
                .map(temp -> {
                    ProjectDto projectDto = new ProjectDto();
                    projectDto.setCreationTime(temp.getCreationTime());
                    projectDto.setExpiryTime(temp.getExpiryTime());
                    projectDto.setDescription(temp.getDescription());
                    projectDto.setPriority(temp.getPriority());
                    projectDto.setUserId(temp.getUser().getId());
                    return projectDto;
                }).collect(Collectors.toList());
        return projectDtos;
    }
}
