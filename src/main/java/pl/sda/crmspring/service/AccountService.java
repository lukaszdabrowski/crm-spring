package pl.sda.crmspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.crmspring.model.dto.AccountDto;
import pl.sda.crmspring.model.dto.AddressDto;
import pl.sda.crmspring.model.dto.RequestAccountAndAddress;
import pl.sda.crmspring.model.entity.Account;
import pl.sda.crmspring.model.entity.Address;
import pl.sda.crmspring.model.entity.AppUser;
import pl.sda.crmspring.repository.AccountRepository;
import pl.sda.crmspring.repository.AddressRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    AppUserService appUserService;

    @Autowired
    AddressService addressService;

    @Autowired
    AppUserAuthenticationService appUserAuthenticationService;


    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Optional<Account> addAccount(AccountDto accountDto) {
        Optional<Account> accountOptional = accountRepository.findByName(accountDto.getName());
        if (!accountOptional.isPresent()) {
            Account account = new Account();
            account.setName(accountDto.getName());
            account.setImportance(accountDto.getImportance());
            Optional<AppUser> optionalAppUser = appUserService.findById(accountDto.getUserId());
            if (optionalAppUser.isPresent()) {
                account.setUser(optionalAppUser.get());
            }
            return Optional.of(accountRepository.save(account));
        }
        return Optional.empty();
    }

    public List<AccountDto> getAllAccounts() {
        List<Account> accountOptionals = accountRepository.findAll();
        List<AccountDto> accountDtos = accountOptionals
                .stream()
                .map(temp -> {
                    AccountDto accountDto = new AccountDto();
                    accountDto.setName(temp.getName());
                    accountDto.setUserId(temp.getUser().getId());
                    accountDto.setImportance(temp.getImportance());
                    return accountDto;
                }).collect(Collectors.toList());
        return accountDtos;
    }

    public Optional<AccountDto> getAccountById(Long id) {
        Optional<Account> accountOptional = accountRepository.findById(id);
        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            AccountDto accountDto = new AccountDto();
            accountDto.setName(account.getName());
            accountDto.setUserId(account.getUser().getId());
            accountDto.setImportance(account.getImportance());
            return Optional.of(accountDto);
        }

        return Optional.empty();
    }

    public Optional<Account> addAccountWithAddress(RequestAccountAndAddress requestAccountAndAddress) {
        Optional<Account> accountOptional = accountRepository.findByName(requestAccountAndAddress.getName());
        if (!accountOptional.isPresent()) {
            Account account = new Account();
            Address address = new Address();
            account.setName(requestAccountAndAddress.getName());
            account.setImportance(requestAccountAndAddress.getImportance());
            Optional<AppUser> optionalAppUser = Optional.of(appUserAuthenticationService.getLoggedInUser().get());
            if (optionalAppUser.isPresent()) {
                account.setUser(optionalAppUser.get());
            }
            address.setStreet(requestAccountAndAddress.getStreet());
            address.setHouseNumber(requestAccountAndAddress.getHouseNumber());
            address.setPostCode(requestAccountAndAddress.getPostCode());
            address.setCity(requestAccountAndAddress.getCity());
            addressRepository.save(address);
            return Optional.of(accountRepository.save(account));
        }
        return Optional.empty();
    }



}
