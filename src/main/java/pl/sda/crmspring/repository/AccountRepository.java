package pl.sda.crmspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.crmspring.model.entity.Account;
import pl.sda.crmspring.model.entity.AppUser;

import java.util.Optional;


public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByName (String name);
}
