package pl.sda.crmspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.crmspring.model.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
