package pl.sda.crmspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.crmspring.model.entity.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {

}
