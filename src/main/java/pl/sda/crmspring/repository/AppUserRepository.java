package pl.sda.crmspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.crmspring.model.entity.AppUser;
import pl.sda.crmspring.model.entity.UserRole;

import java.util.Optional;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByUsername (String name);
}
