package pl.sda.crmspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.crmspring.model.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
