package pl.sda.crmspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.crmspring.model.entity.Address;
import pl.sda.crmspring.model.entity.AppUser;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long> {
    Optional<Address> findByCityAndAndStreetAndHouseNumber (String city, String street, int houseNumber);
}
