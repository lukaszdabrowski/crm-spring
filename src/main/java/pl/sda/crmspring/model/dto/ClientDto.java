package pl.sda.crmspring.model.dto;

import lombok.*;
import pl.sda.crmspring.model.entity.types.TypeForClient;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {

    private Long accountId;
    private String firstName;
    private String lastName;
    private TypeForClient type;
    private String position;
}
