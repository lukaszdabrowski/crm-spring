package pl.sda.crmspring.model.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDto {

    private String description;
    private LocalDateTime creationTime;
    private LocalDateTime expiryTime;
    private int priority;
    private Long userId;
}
