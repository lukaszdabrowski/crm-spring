package pl.sda.crmspring.model.dto;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {

    private String street;
    private int houseNumber;
    private String postCode;
    private String city;
}
