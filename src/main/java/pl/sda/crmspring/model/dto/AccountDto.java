package pl.sda.crmspring.model.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {

    private String name;
    private int importance;
    private Long userId;
}
