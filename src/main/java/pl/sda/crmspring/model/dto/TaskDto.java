package pl.sda.crmspring.model.dto;


import lombok.*;
import pl.sda.crmspring.model.entity.types.TypeForTask;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private String description;
    private LocalDateTime creationTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private int priority;
    private TypeForTask type;
    private ClientDto client;
    private ProjectDto project;
}
