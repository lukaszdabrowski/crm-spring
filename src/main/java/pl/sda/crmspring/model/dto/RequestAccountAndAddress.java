package pl.sda.crmspring.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestAccountAndAddress {
    private String name;
    private int importance;
    private Long userId;
    private String street;
    private int houseNumber;
    private String postCode;
    private String city;
}
