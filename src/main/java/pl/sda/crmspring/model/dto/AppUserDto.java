package pl.sda.crmspring.model.dto;


import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppUserDto {

    private String username;
    private String password;
    private String firstName;
    private String lastName;
}
