package pl.sda.crmspring.model.entity;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import pl.sda.crmspring.model.entity.types.TypeForTask;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String description;
    @CreationTimestamp
    private LocalDateTime creationTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private int priority;
    private TypeForTask type;
    @ManyToOne
    private Client client;
    @ManyToOne
    private Project project;
    @ManyToOne
    private AppUser user;

}
