package pl.sda.crmspring.model.entity;

import lombok.*;
import pl.sda.crmspring.model.entity.types.TypeForClient;

import javax.persistence.*;
import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Account account;
    private String firstName;
    private String lastName;
    private TypeForClient type;
    private String position;
    private String email;
    private String telephone;
    @OneToMany(mappedBy = "client")
    private List<Task> tasks;

}
