package pl.sda.crmspring.model.entity.types;

public enum TypeForClient {
    UNIMPORTANT,NORMAL, IMPORTANT, VIP;
}
