package pl.sda.crmspring.model.entity.types;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;


public enum TypeForTask {
    VISIT,PHONE_CALL,PRESENTATION,HOME_OFFICE;
}
