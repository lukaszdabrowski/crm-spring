package pl.sda.crmspring.model.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String description;
    @CreationTimestamp
    private LocalDateTime creationTime;
    private LocalDateTime expiryTime;
    private int priority;
    @OneToMany(mappedBy = "project")
    private List<Task> tasks;
    @ManyToOne
    private AppUser user;
}
