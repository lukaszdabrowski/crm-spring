package pl.sda.crmspring.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    @OneToMany
    private List<Task> tasks;
    @OneToMany
    private List<Account> accounts;
    @OneToMany
    private List<Project> projects;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<UserRole> roles;
}
