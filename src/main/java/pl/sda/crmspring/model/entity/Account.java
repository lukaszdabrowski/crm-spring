package pl.sda.crmspring.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @OneToOne
    private  Address address;
    private int importance;
    @OneToMany(mappedBy = "account")
    private List<Client> clients;
    @OneToMany
    private List<Project> projects;
    @ManyToOne
    private AppUser user;

}
