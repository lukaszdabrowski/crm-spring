package pl.sda.crmspring.model.entity.types;

public enum  TypeForAccount {
    PROSPECT, CURRENT;
}
